user www-data;
worker_processes auto;
worker_rlimit_nofile 80000;
pid /run/nginx.pid;
thread_pool mine threads=512 max_queue=0;

events {
	worker_connections  1024;
	worker_aio_requests 512;
	multi_accept        on;
	use                 epoll;
	accept_mutex off;
}
http {
	upstream memcached-servers {
		server 127.0.0.1:11211;
	}
	upstream php-fpm-lb {
		server 127.0.0.1:11311;
        keepalive 32;
	}
	error_log /var/log/nginx-error.log error;
	sendfile on;
	aio threads=mine;
	sendfile_max_chunk 256k;
	keepalive_timeout 15s;
    keepalive_requests 100000;
	types_hash_max_size 2048;
	fastcgi_read_timeout 3600;
	include /etc/nginx/mime.types;
	default_type application/octet-stream;
	ignore_invalid_headers on;
	client_max_body_size 128M;
	client_body_buffer_size 15m;
	client_header_timeout 5s;
	client_body_timeout 5s;
	send_timeout 5s;
	connection_pool_size 256;
	client_header_buffer_size 4k;
	large_client_header_buffers 4 32k;
	request_pool_size 4k;
	output_buffers 4 32k;
	open_file_cache max=80000 inactive=20s;
	open_file_cache_valid 30s;
	open_file_cache_min_uses 2;
	open_file_cache_errors on;
	map $scheme $fastcgi_https {
		default off;
		https on;
	}
	brotli on;
	brotli_static on;
	brotli_types *;
	fastcgi_cache_key "$scheme$request_method$host$request_uri";
	include /etc/nginx/all-site-security.conf;
	include /etc/nginx/sites-enabled/*;
	include /home/*/site-config/*;
}
